package com.hobbit.ui;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.hobbit.framework.Suite;
import com.hobbit.pageobject.Homepage;
import com.hobbit.util.Navigate;

/**
 * UI test for validating Bing homepage
 * 
 * @author Mirzafazulur Rahamanbaig
 *
 */
public class ValidateHomepageTest extends Suite {

	/**
	 * Validate homepage test
	 */
	@Test
	public void validateHomepageTest() {

		WebDriver driver = Navigate.getHomepage();

		Homepage homepage = PageFactory.initElements(driver, Homepage.class);
		homepage.verifyNavigationBar();

		driver.quit();
	}
}
