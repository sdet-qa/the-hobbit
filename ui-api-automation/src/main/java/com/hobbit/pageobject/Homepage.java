package com.hobbit.pageobject;

import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.hobbit.constant.BingConstants;
import com.hobbit.framework.Assert;
import com.hobbit.framework.Suite;

/**
 * Page object for Bing homepage
 * 
 * @author Mirzafazulur Rahamanbaig
 *
 */
public class Homepage {

	private Logger LOG;
	private WebDriver driver;

	public Homepage(WebDriver driver) {

		LOG = Suite.getLogger();

		this.driver = driver;

		isValidPageState();
	}

	private void isValidPageState() {

		LOG.info("Homepage#isValidPageState method");

		String title = driver.getTitle();
		Assert.assertTrue(BingConstants.BingHomepageTitle.contains(title), "Homepage title is- " + title);

		WebElement searchBox = driver.findElement(By.name("q"));
		Assert.assertTrue(searchBox.isDisplayed(), "Search box is not visible");

		WebElement accountMenu = driver.findElement(By.id("id_h"));
		Assert.assertTrue(accountMenu.isDisplayed(), "Top right account menu is not visible");
	}

	public void verifyNavigationBar() {

		LOG.info("Inside verifyNavigationBar method");

		WebElement languageBar = driver.findElement(By.id("sb_form_q"));
		Assert.assertTrue(languageBar.isDisplayed(), "Language bar is not visible");
	}

}
