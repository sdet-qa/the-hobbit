package com.hobbit.constant;

/**
 * Bing constants
 * 
 * @author Mirzafazulur Rahamanbaig
 *
 */
public interface BingConstants {

	public String BingHomepageTitle = "Bing";
}
