package com.hobbit.constant;

public enum DriverType {

	Chrome, Firefox, BrowserStack, HtmlUnit, PhantomJS
}
