package com.hobbit.framework;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.hobbit.constant.DriverType;

/**
 * Suite is base class for the framework, every test class must extend this base
 * class
 * 
 * @author Mirzafazulur Rahamanbaig
 *
 */
public class Suite {

	private static String environment;
	private static DriverType driverType;
	private static List<WebDriver> drivers;
	private static Logger LOG;

	/**
	 * BeforeSuite initialize suite
	 * 
	 * @param env
	 * @param drType
	 */
	@BeforeSuite
	@Parameters({ "env", "drType" })
	public static void initSuite(@Optional String env, @Optional String drType) {

		// init logger
		LOG = Logger.getLogger("the-hobbit");
		LOG.info("In BeforeSuite, Logger initalized");

		// set environment
		if (env == null || "qa".equals(env)) {

			environment = "https://www.bing.com";
		} else if ("prod".equals(env)) {

			environment = "https://www.bing.co.in";
		}

		LOG.info("Environment initialized- " + environment);

		// set driver type
		if (drType == null || "chrome".equals(drType)) {

			driverType = DriverType.Chrome;
		} else if ("firefox".equals(drType)) {

			driverType = DriverType.Firefox;
		}

		LOG.info("Driver initialized- " + driverType);

		// init list to quit after suite
		drivers = new ArrayList<WebDriver>();
	}

	/**
	 * Tear down after suite
	 */
	@AfterSuite
	public void tearDown() {

		LOG.info("In AfterSuite, tearDown method");

		for (WebDriver driver : drivers) {

			try {

				driver.quit();
			} catch (Exception e) {

				LOG.info("Exception while quitting WebDriver");
			}
		}
	}

	/**
	 * Get environment
	 * 
	 * @return
	 */
	public static String getEnvironment() {

		return environment;
	}

	/**
	 * Get driver type
	 * 
	 * @return
	 */
	public static DriverType getDriverType() {

		return driverType;
	}

	/**
	 * Get logger
	 * 
	 * @return
	 */
	public static Logger getLogger() {

		return LOG;
	}

	/**
	 * Get drivers
	 * 
	 * @return
	 */
	public static List<WebDriver> getDrivers() {

		return drivers;
	}
}
