package com.hobbit.framework;

import java.util.logging.Logger;

/**
 * Wrapper class for org.testng.Assert
 * 
 * @author Mirzafazulur Rahamanbaig
 *
 */
public class Assert {

	/**
	 * Assert true
	 * 
	 * @param condition
	 * @param message
	 */
	public static void assertTrue(boolean condition, String message) {

		Logger LOG = Suite.getLogger();

		if (!condition) {

			LOG.info("Assertion failed- " + message);

			org.testng.Assert.assertTrue(condition, message);
		}
	}
}
