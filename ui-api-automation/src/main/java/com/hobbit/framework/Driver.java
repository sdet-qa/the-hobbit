package com.hobbit.framework;

import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.hobbit.constant.DriverType;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * Driver class to instantiate based on suite param
 * 
 * @author Mirzafazulur Rahamanbaig
 *
 */
public class Driver {

	/**
	 * Get WebDriver
	 * 
	 * @return
	 */
	public static WebDriver getDriver() {

		Logger LOG = Suite.getLogger();
		WebDriver driver = null;
		DriverType driverType = Suite.getDriverType();

		if (driverType == DriverType.Chrome) {

			WebDriverManager.chromedriver().setup();

			driver = new ChromeDriver();
		} else if (driverType == DriverType.Firefox) {

			WebDriverManager.firefoxdriver().setup();

			driver = new FirefoxDriver();
		}

		Suite.getDrivers().add(driver);
		driver.manage().window().maximize();

		LOG.info("Instantiated " + driverType + " WebDriver");

		return driver;
	}
}
