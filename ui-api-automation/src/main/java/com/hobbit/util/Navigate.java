package com.hobbit.util;

import org.openqa.selenium.WebDriver;

import com.hobbit.framework.Driver;
import com.hobbit.framework.Suite;

/**
 * Utility to navigate across the application
 * 
 * @author Mirzafazulur Rahamanbaig
 *
 */
public class Navigate {

	public static WebDriver getHomepage() {

		WebDriver driver = Driver.getDriver();
		driver.get(Suite.getEnvironment());

		return driver;
	}
}
