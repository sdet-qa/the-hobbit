# The Hobbit

Parameters to be set before running test suite

| Parameter | Description |
| ------ | ------ |
| env | Environment on which suite executes, default is **qa** <br /> Available environments are **qa** and **prod**|
| drType | Driver type which suite executes UI tests on, default is **Chrome** <br /> Available driver types are **Chrome**, **Firefox** and **BrowserStack**|

Command line eg: 
*  *mvn test -Denv=qa -DdrType=chrome*
*  *mvn test -DdrType=firefox*
*  *mvn test -Denv=prod*
